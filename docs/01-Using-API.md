---
tags: [using api]
---

# Using API

Wflow API allow developers to request information from the wflow platform including but not limited to documents details, download documents or files, etc. as well as perform actions on the wflow platform on a user’s behalf, such as, creating a new document or update existing documents, upload files.

---

## Athentication

Most of HTTP request made to wflow API must be authenticated by wflow. Wflow supports for request authentication **OAuth 2.0**.

### Using OAuth 2.0
[OAuth 2.0](http://tools.ietf.org/html/rfc6749) allows applications to obtain access to wflow resources.

The following sections will provide an overview on the OAuth protocol.

#### BIG PICTURE
The most common interactions are:

- Browsers communicate with web applications
-	Web applications communicate with web APIs (sometimes on their own, sometimes on behalf of a user)
-	Browser-based applications communicate with web APIs
-	Native applications communicate with web APIs
-	Server-based applications communicate with web APIs
-	Web APIs communicate with web APIs (sometimes on their own, sometimes on behalf of a user)

Typically each and every layer (front-end, middle-tier and back-end) has to protect resources and implement authentication and/or authorization – often against the same user store.
Outsourcing these fundamental security functions to a security token service prevents duplicating that functionality across those applications and endpoints.
Restructuring the application to support a security token service leads to the following architecture and protocols:

![Web app architecture with OAuth 2](../assets/images/webapp_architecture_with_oauth2.png)

Such a design divides security concerns into two parts:</br>

##### AUTHENTICATION
Authentication is needed when an application needs to know the identity of the current user. Typically these applications manage data on behalf of that user and need to make sure that this user can only access the data for which he is allowed. The most common example for that is (classic) web applications – but native and JS-based applications also have a need for authentication.
The most common authentication protocols are SAML2p, WS-Federation and OpenID Connect – SAML2p being the most popular and the most widely deployed.
OpenID Connect is the newest of the three, but is considered to be the future because it has the most potential for modern applications. It was built for mobile application scenarios right from the start and is designed to be API friendly.</br>

##### API ACCESS
Applications have two fundamental ways with which they communicate with APIs – using the application identity, or delegating the user’s identity. Sometimes both methods need to be combined.
OAuth2 is a protocol that allows applications to request access tokens from a security token service and use them to communicate with APIs. This delegation reduces complexity in both the client applications as well as the APIs since authentication and authorization can be centralized.

#### TERMINOLOGY
The specs, documentation and object model use a certain terminology that you should be aware of.
![OAuth 2 schema](../assets/images/oauth2_schema.png)

##### IDENTITY SERVER
Identity server is an OpenID Connect provider - it implements the OpenID Connect and OAuth 2.0 protocols.
Different literature uses different terms for the same role - you probably also find security token service, identity provider, authorization server, IP-STS and more.
But they are in a nutshell all the same: a piece of software that issues security tokens to clients.
Identity server has a number of jobs and features - including:
*	protect your resources
*	authenticate users using a local account store or via an external identity provider
*	provide session management and single sign-on
*	manage and authenticate clients
*	issue identity and access tokens to clients
*	validate tokens
</br>
##### USER
A user is a human that is using a registered client to access resources.
</br>
##### CLIENT
A client is a piece of software that requests tokens from Identity server - either for authenticating a user (requesting an identity token) or for accessing a resource (requesting an access token). A client must be first registered with Identity server before it can request tokens.
Examples for clients are web applications, native mobile or desktop applications, SPAs, server processes etc.
</br>
##### RESOURCES
Resources are something you want to protect with Identity server - either identity data of your users, or APIs.
Every resource has a unique name - and clients use this name to specify to which resources they want to get access to.
Identity data Identity information (aka claims) about a user, e.g. name or email address.
APIs APIs resources represent functionality a client wants to invoke - typically modeled as Web APIs, but not necessarily.
</br>
##### IDENTITY TOKEN
An identity token represents the outcome of an authentication process. It contains at a bare minimum an identifier for the user (called the sub aka subject claim) and information about how and when the user authenticated. It can contain additional identity data.
</br>
##### ACCESS TOKEN
An access token allows access to an API resource. Clients request access tokens and forward them to the API. Access tokens contain information about the client and the user (if present). APIs use that information to authorize access to their data.

#### SUPPORTED SPECIFICATIONS
Wflow identity server implements the following specifications:
</br>
##### OPENID CONNECT
*	OpenID Connect Core 1.0 [[(spec)]](http://openid.net/specs/openid-connect-core-1_0.html)
*	OpenID Connect Discovery 1.0 [(spec)](http://openid.net/specs/openid-connect-discovery-1_0.html)
*	OpenID Connect Session Management 1.0 - draft 28 [(spec)](http://openid.net/specs/openid-connect-session-1_0.html)
*	OpenID Connect Front-Channel Logout 1.0 - draft 02 [(spec)](https://openid.net/specs/openid-connect-frontchannel-1_0.html)
*	OpenID Connect Back-Channel Logout 1.0 - draft 04 [(spec)](https://openid.net/specs/openid-connect-backchannel-1_0.html)
</br>
##### OAUTH 2.0
*	OAuth 2.0 [(RFC 6749)](http://tools.ietf.org/html/rfc6749)
*	OAuth 2.0 Bearer Token Usage [(RFC 6750)](http://tools.ietf.org/html/rfc6750)
*	OAuth 2.0 Multiple Response Types [(spec)](http://openid.net/specs/oauth-v2-multiple-response-types-1_0.html)
*	OAuth 2.0 Form Post Response Mode [(spec)](http://openid.net/specs/oauth-v2-form-post-response-mode-1_0.html)
*	OAuth 2.0 Token Revocation [(RFC 7009)](https://tools.ietf.org/html/rfc7009)
*	OAuth 2.0 Token Introspection [(RFC 7662)](https://tools.ietf.org/html/rfc7662)
*	Proof Key for Code Exchange [(RFC 7636)](https://tools.ietf.org/html/rfc7636)
*	JSON Web Tokens for Client Authentication [(RFC 7523)](https://tools.ietf.org/html/rfc7523)
*	OAuth 2.0 Device Authorization Grant [(RFC 8628)](https://tools.ietf.org/html/rfc8628)
*	OAuth 2.0 Mutual TLS Client Authentication and Certificate-Bound Access Tokens [(draft)](https://tools.ietf.org/html/draft-ietf-oauth-mtls-13)

#### ABSTRACT PROTOCOL FLOW

![Abstract protocol flow](../assets/images/oauth2_abstract_protocol_flow_schema.png)
Here is a more detailed explanation of the steps in the diagram:


1. The application requests authorization to access service resources from the user
2. If the user authorized the request, the application receives an authorization grant
3. The application requests an access token from the authorization server (API) by presenting authentication of its own identity, and the authorization grant
4. If the application identity is authenticated and the authorization grant is valid, the authorization server (API) issues an access token to the application. Authorization is complete.
5. The application requests the resource from the resource server (API) and presents the access token for authentication
6. If the access token is valid, the resource server (API) serves the resource to the application

The actual flow of this process will differ depending on the authorization grant type in use, but this is the general idea. We will explore different grant types in a later section.

#### APPLICATION (CLIENT) REGISTRATION
Before using OAuth with your application (client), you must register your application with the service. This is done through [customer administration](https://admin.wflow.com)  ([the procedure is described here](#client-registration)) if you use non-interactive authentication. Only designated administrators have access to customer management. If you do not have this permission, ask the appropriate administrator to register. If you want to use interactive authentication, then registration is only possible through technical support. You will need the following information to register (and probably details about your application):

* Application Name
* Application short code for Client ID
* Redirect URI or Callback URL (*in case of interactive application - details a later section*)

The redirect URI is where the service will redirect the user after they authorize (or deny) your application, and therefore the part of your application that will handle authorization codes or access tokens.

After registration you will receive [**Client ID** and **Client secret**](#client-id-and-client-secret).

</br>

##### Client ID and Client secret
Once your application is registered, the service will issue “client credentials” in the form of a client identifier and a client secret. The Client ID is a publicly exposed string that is used by the service API to identify the application, and is also used to build authorization URLs that are presented to users. The Client Secret is used to authenticate the identity of the application to the service API when the application requests to access a user’s account, and must be kept private between the application and the API. Carefully store this data in a safe place. In particular, Client secret, which cannot be traced. If it is lost or betrayed, it must be replaced with a new one.
</br>
##### Client Registration

Before using OAuth with your application (client), you must register your application with the service. You register the client in the [Customer management application](https://admin.wflow.com). Only designated administrators have access to Customer management.
In the [API clients](https://admin.wflow.com/apiclients) section add a new record (press the "plus" button):
</br>
![API Clients](../assets/images/stoplight_register_api_client_001.png)
</br>
Fill in **Client Id**, **Name** and select **OAuth2 grant type**. The grant type can be **Client credentials** (*client_credentials*) (default value), **Authorization code** (*authorization_code*) or **Implicit** (*implicit*).:
</br>
![Crate client](../assets/images/stoplight_register_api_client_002.png)
</br>
If you select **Authorization code** or **Implicit** grant type, you must also specify a list of allowed redirect URIs in **Redirect URI whitelist**. This is list of valid URIs for your client / application separated by a space or line break. The redirect_uri parameter for all OAuth2 API calls will be verified againts this list:
</br>
![Crate client](../assets/images/stoplight_register_api_client_002_a.png)
</br>
After successful saving, a window with **Full Client Id** and **Client secret** is displayed. This data is then used for authentication and authorization in OAuth:
</br>
![New client Id and Secret](../assets/images/stoplight_register_api_client_003.png)
</br>
The Client is thus registered and everything is ready to [allow integration into a specific organization](#enabling--disabling-client-integration-in-the-organization). The new client is displayed in the list. In the event of a Client secret loss or leak, you can either perform a Refresh secret or delete the Client from this list.
</br>
##### Enabling / disabling Client integration in the organization (for Client credentials grant type only)

In order for the Client to have access to the organization's data, the organization's administrator must allow this access. Client access control is performed in the organization workspace in **Settings** in the Integration section by checking / unchecking the relevant Client. This only applies to clients with the **Client credentials** grant type. Clients with **Authorization code** or **Implicit** grant type do not need to be granted access to the organization. In their case, access is determined by the account under which they log in:
</br>
![Crate client](../assets/images/stoplight_allow_api_client_access_001.png)

#### GRANT TYPES

Grant types are a way to specify how a client wants to interact with IdentityServer. The OpenID Connect and OAuth 2 specs define the following grant types:

* Client credentials
* Authorization code
* Implicit
* Hybrid
* Resource owner password
* Device flow
* Refresh tokens
* Extension grants

Wflow API supports only these grant types:

* Client credentials
* Authorization code
* Implicit
* Hybrid
</br>
##### MACHINE TO MACHINE COMMUNICATION
This is the simplest type of communication. Tokens are always requested on behalf of a client, no interactive user is present.
In this scenario, you send a token request to the token endpoint using the **Client credentials ("client_credentials")** grant type. The client typically has to authenticate with the token endpoint using its client ID and secret.

The application requests an access token by sending its credentials, its client ID and client secret (in request body), to the authorization server.
If the application credentials check out, the authorization server returns an access token to the application. Now the application is authorized to use its own account!

You can try to aquire access token in example POST request [here](#try-it).

Code samples:
<!--
type: tab
title: Java script
-->
```json
var data = "client_id=your_wflow_non_interactive_client_name" +
"&client_secret=your_secret" + 
"&grant_type=client_credentials" + 
"&scope=uccl_common_api";

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "https://account.wflow.com/connect/token");
xhr.setRequestHeader("content-type", "multipart/form-data");

xhr.send(data);
```
<!--
type: tab
title: C#
-->
```json
var client = new HttpClient();
var request = new HttpRequestMessage
{
    Method = HttpMethod.Post,
    RequestUri = new Uri('https://account.wflow.com/connect/token"),
    Content = new FormUrlEncodedContent(new Dictionary<string, string>
    {
        { "client_id", "your_wflow_non_interactive_client_name" },
        { "client_secret", "your_secret" },
        { "grant_type", "client_credentials" },
        { "scope", "uccl_common_api" },
    }),
};
using (var response = await client.SendAsync(request))
{
    response.EnsureSuccessStatusCode();
    var body = await response.Content.ReadAsStringAsync();
}
```
<!-- type: tab-end -->

##### INTERACTIVE CLIENTS
This is the most common type of client scenario: web applications, SPAs or native/mobile apps with interactive users.
For this type of clients, the authorization_code flow was designed. That flow consists of two physical operations:
*	a front-channel step via the browser where all “interactive” things happen, e.g. login page, consent etc. This step results in an authorization code that represents the outcome of the front-channel operation.
*	a back-channel step where the authorization code from step 1 gets exchanged with the requested tokens. Confidential clients need to authenticate at this point.
This flow has the following security properties:
*	no data (besides the authorization code which is basically a random string) gets leaked over the browser channel
*	authorization codes can only be used once
*	the authorization code can only be turned into tokens when (for confidential clients - more on that later) the client secret is known
</br>
###### Grant Type: Authorization Code
The authorization code grant type is the most commonly used because it is optimized for server-side applications, where source code is not publicly exposed, and Client Secret confidentiality can be maintained. This is a redirection-based flow, which means that the application must be capable of interacting with the user-agent (i.e. the user’s web browser) and receiving API authorization codes that are routed through the user-agent.

![Authorization Code Flow](../assets/images/oauth2_auth_code_flow_schema.png)

Step 1: Authorization Code Link
With the Authorization Code grant type, the user is presented with an authorization code link, that requests a code from the API.

Step 2: User Authorizes Application
The user must first log in to the service, to authenticate his identity (unless they are already logged in). Then he will be prompted by the service to authorize or deny the client / application access to his account.

Step 3: Application Receives Authorization Code
If the user clicks agree with application authorization, the service redirects the user-agent to the application redirect URI, which was specified during the client registration, along with an authorization code.

Step 4: Application Requests Access Token
The application requests an access token from the API, by passing the authorization code along with authentication details, including the client secret, to the API token endpoint.

Step 5: Application Receives Access Token
If the authorization is valid, the API will send a response containing the access token (and optionally, a refresh token - "offline_access" scope must be presented) to the application.

Now the application is authorized! It may use the token to access the user’s account via the service API, limited to the scope of access, until the token expires or is revoked. If a refresh token was issued, it may be used to request new access tokens if the original token has expired.
</br>
###### Grant type: Implicit
The implicit grant type is used for mobile apps and web applications (i.e. applications that run in a web browser), where the client secret confidentiality is not guaranteed. The implicit grant type is also a redirection-based flow but the access token is given to the user-agent to forward to the application, so it may be exposed to the user and other applications on the user’s device. Also, this flow does not authenticate the identity of the application, and relies on the redirect URI (that was registered with the service) to serve this purpose.

The implicit grant type does not support refresh tokens.

The implicit grant flow basically works as follows: the user is asked to authorize the application, then the authorization server passes the access token back to the user-agent, which passes it to the application.

![Implicit flow](../assets/images/oauth2_implicit_flow_schema.png)

Step 1: Implicit Authorization Link
With the implicit grant type, the user is presented with an authorization link, that requests a token from the API.

Step 2: User Authorizes Application
When the user clicks the link, they must first log in to the service, to authenticate their identity (unless they are already logged in).

Step 3: User-agent Receives Access Token with Redirect URI
The service redirects the user-agent to the application redirect URI, and includes a URI fragment containing the access token.

Step 4: User-agent Follows the Redirect URI
The user-agent follows the redirect URI but retains the access token.

Step 5: Application Sends Access Token Extraction Script
The application returns a webpage that contains a script that can extract the access token from the full redirect URI that the user-agent has retained.

Step 6: Access Token Passed to Application
The user-agent executes the provided script and passes the extracted access token to the application.

Now the application is authorized. It may use the token to access the user’s account via the service API, limited to the scope of access, until the token expires or is revoked.

Code sample:
<!--
type: tab
title: Java script
-->
```json
import { UserManager, WebStorageStateStore } from "oidc-client";
const clientId = "your_wflow_interactive_client_name";
const authUrl = "https://account.wflow.com";
const originUrl = window.location.origin;
const redirect_url = originUrl + "/auth";
const INITIAL_AUTHWELLKNOWN = {
  issuer: authUrl,
  jwks_uri: authUrl + "/.well-known/openid-configuration/jwks",
  authorization_endpoint: authUrl + "/connect/authorize",
  token_endpoint: authUrl + "/connect/token",
  userinfo_endpoint: authUrl + "/connect/userinfo",
  end_session_endpoint: authUrl + "/connect/endsession",
  check_session_iframe: authUrl + "/connect/checksession",
  revocation_endpoint: authUrl + "/connect/revocation",
  introspection_endpoint: authUrl + "/connect/introspect",
};
const settings = {
  userStore: new WebStorageStateStore({ store: window.localStorage }),
  authority: authUrl,
  filterProtocolClaims: true,
  stsServer: authUrl,
  redirect_uri: redirect_url,
  client_id: clientId,
  response_type: "code",
  scope: "openid profile uccl_api wflow_api uccl_common_api",
  post_logout_redirect_uri: originUrl,
  forbidden_route: "/forbidden",
  unauthorized_route: "/unauthorized",
  silent_renew: false,
  silent_renew_url: originUrl,
  history_cleanup_off: true,
  auto_userinfo: true,
  log_console_warning_active: true,
  log_console_debug_active: true,
  max_id_token_iat_offset_allowed_in_seconds: 86400,
  metadata: INITIAL_AUTHWELLKNOWN,
};
const userManager = new UserManager(settings);
const getUser = () => {
  return userManager.getUser();
};
const login = () => {
  return userManager.signinRedirect();
};
const logout = () => {
  return userManager.signoutRedirect({
    externalUrl: originUrl,
    externalRedirect: originUrl,
  });
};
export default class AuthService {
  constructor() {
    this.getUser = getUser;
    this.login = login;
    this.logout = logout;
  }
}
```
<!-- type: tab-end -->

#### SUMMARY
Interactive clients should use an authorization code-based flow. To protect against code substitution, either hybrid flow or PKCE should be used. If PKCE is available, this is the simpler solution to the problem.
PKCE is already the official recommendation for native applications and SPAs - and with the release of ASP.NET Core 3 also by default supported in the OpenID Connect handler as well.
#### WFLOW IDENTITY SERVER ENDPOINTS
Server base URL https://account.wflow.com
</br>
##### Main endpoints </br>

###### Discovery endpoint
URL https://account.wflow.com/.well-known/openid-configuration

Returns list of all server endpoints in JSON.

```json http
{
  "method": "get",
  "url": "https://account.wflow.com/.well-known/openid-configuration"
}
```
###### Token endpoint
URL https://account.wflow.com/connect/token

The token endpoint can be used to programmatically request tokens. It supports the `password`, `authorization_code`, `client_credentials`, `refresh_token` grant types.

>Note
>
>Wflow identity server supports a subset of the OpenID Connect and OAuth 2.0 token request parameters.

**`client_id`**

**`client identifier`** (required)

**`client_secret`** client secret either in the post body, or as a basic authentication header. Optional.

**`grant_type`**
  * `authorization_code`
  * `client_credentials`
  * `refresh_token`

**`scope`** one or more registered scopes. If not specified, a token for all explicitly allowed scopes will be issued.

**`redirect_uri`** required for the authorization_code grant type

**`code`** the authorization code (required for authorization_code grant type)

**`code_verifier`** PKCE proof key

**`refresh_token`** the refresh token (required for refresh_token grant type)

#### Try it:

You can try to aquire access token here. Fill in the following information in the widget:

**Headers section:**

Select **x-www-form-urlencoded** option in **Body section** and agree with Content-Type setting in header or set manually in Headers section:

Key | Value 
---------|----------
 Content-Type | **`application/x-www-form-urlencoded`** |

**Body section:**


Key | Value | Note
---------|----------|----------
 client_id | *your_wflow_non_interactive_client_name* | if you don't already have it, ask the customer administrator to create one or ask for technical support
 client_secret | *your_secret* | if you don't already have it, ask the customer administrator to create one or ask for technical support
 grant_type | **`client_credentials`** ||
 scope | **`uccl_common_api`** ||

```json http
{
  "method": "post",
  "url": "https://account.wflow.com/connect/token",
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "body": "client_id=your_wflow_non_interactive_client_name&client_secret=your_secret&grant_type=client_credentials&scope=uccl_common_api"
}
```
>Note
>
>Copy and paste the access token from the response to [jwt.io](https://jwt.io/) to inspect the raw token.

## API Requests
All API requests must be made over HTTPS. The complete URL varies depending on the resource being accessed.

For instance, to  in your app, you must make an HTTP GET request to this URL: https://admin.wflow.com/api/organizations/{organizationId}. If your app is registered as OAuth app, your app must have the **uccl_admin_api** scope in order to use the admin API.

Example (partial code - Type script)
```json
/**
  * Gets the organization.
  * 
  * @param organizationId The organization identifier.
  * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
  * @param reportProgress flag to report request and response progress.
  */
public getOrganization(organizationId: string, observe?: 'body', reportProgress?: boolean): Observable<Organization>;
public getOrganization(organizationId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Organization>>;
public getOrganization(organizationId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Organization>>;
public getOrganization(organizationId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

    if (organizationId === null || organizationId === undefined) {
        throw new Error('Required parameter organizationId was null or undefined when calling getOrganization.');
    }

    let headers = this.defaultHeaders;

    // authentication (oauth2) required
    if (this.configuration.accessToken) {
        const accessToken = typeof this.configuration.accessToken === 'function'
            ? this.configuration.accessToken()
            : this.configuration.accessToken;
        headers = headers.set('Authorization', 'Bearer ' + accessToken);
    }

    // to determine the Accept header
    let httpHeaderAccepts: string[] = [
        'text/plain',
        'application/json',
        'text/json'
    ];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    if (httpHeaderAcceptSelected != undefined) {
        headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [
    ];

    return this.httpClient.get<Organization>(`${this.basePath}/api/Organizations/${encodeURIComponent(String(organizationId))}`,
        {
            withCredentials: this.configuration.withCredentials,
            headers: headers,
            observe: observe,
            reportProgress: reportProgress
        }
    );
}

.
.
.

private getDetailData(id: string)
{
  this.organizationsService.getOrganization(id).subscribe(data => {
    console.log(data);
  });
}

```
### Limitations
</br>
The API call has the limits on the number of requests. Their settings or information about their drawing are included in each response in the following headers. For example:
</br>
</br>
**x-rate-limit-period**: 00:01:00 - *time for which the number of requests limit is calculated*
</br>
**x-rate-limit-remaining**: 99 - *number of requests that remain to run out of time*

## Need support ?

Please email us at [developersupport@wflow.com](mailto:developersupport@wflow.com)


