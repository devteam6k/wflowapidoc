# Pagination

Pagination of wflow APIs is the process of organizing the response results of an API call into multiple pages. When you’re making GET calls, especially bulk requests using the “list” API methods, there is a high possibility of a large number of records being returned. To make handling large payloads easier, we provide you with a way to control the maximum number of responses through pagination.

For instance, in the [My organizations list](), you can see that there are two query parameters to help you with pagination:

`page_size`: The number of records that you would like to get in the response per page.

`page_number`: The specific page that you would like to view from all the pages containing response records.

Wflow APIs have a predetermined default and maximum values for `page_size` and `page_number` ; however, we strongly recommend that you set the value for these parameters on your own based on your needs.
Once you make the request, you will be able to see how many pages there are for the request from the `page_count` parameter included in the response.

The response will also include the `page_number` field that shows which page you’re currently viewing the results from. For example, if in the response, the value of `page_count` is 2 and the value of `page_number` is 1, you can paginate through the response and view results for the second page by making another request by setting the value of `page_number` query parameter as 2.

In the sample request below, the value of `page_size` is set to 1 which means in our request to , we are stating that we only want one record to be displayed in the response.

```json
var request = require("request");

var options = {
 method: 'GET',
 url: 'https://api.wflow.com/api/user/myorganizations',
 qs: {
  page_number: '1',
  page_size: '2',
 },
 headers: {
  authorization: 'Bearer weguzcu123wqd...'
 }
};

request(options, function(error, response, body) {
 if (error) throw new Error(error);

 console.log(body);
});
```
In the response above, note that the total number of records for this query is 2. However, only one record is displayed because we specified 1 as the value of the `page_size` query parameter. To view the second record with `page_size` as 1, you will need to make another request similar to the one above, with `page_number` set to 2.

---
## Need support ?

Please email us at [developersupport@wflow.com](mailto:developersupport@wflow.com)


