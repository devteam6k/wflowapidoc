---
tags: [errors]
---

# Errors description

The Wflow API uses HTTP Status codes to reflect a successful or unsuccesful request. 2XX status codes represent a successful request, 4XX/5XX status codes represent an error took place. If you receive an error status code, check the body for an error code and message.

| Status Code | Description        | Most Likely Cause                                                      |
| ----------- | ------------------ | ---------------------------------------------------------------------- |
| 2xx         | Successful Request |                                                                        |
| 400         | Bad Request        | Invalid/missing data                                                   |
| 401         | Unauthorized       | Invalid/missing credentials                                            |
| 403         | Forbidden          | Trying to do forbidden operation ex. lack of user permissions          |
| 404         | Not Found          | The resource dosen’t exists, ex. invalid/non-existent document id      |
| 429         | Too Many Requests  | Hit an API rate limit      |
| 550         | Expected error     | Trying to do invalid operation ex. attempt to update a locked document |
| 551         | Bulk error         | As 550 for bulk operations. Returns array of errors                                             |
| 555         | Unexpected error   |                                                                        |

Error response example

```json
{
  "code": 550,
  "message": "Document locked"
}

```
Bulk error response example

```json
{
  "code": 551,
  "errors": [
    {
      "key": "660e5585-482c-4798-b3c0-a1a368a01f87",
      "value": "Document locked"
    },
    {
      "key": "15a8c5d0-a9b2-43a9-9e0c-7865c0e48e5e",
      "value": "Document locked"
    }
  ]
}
```
