# Try it out

Each API endpoint includes a testing tool widget within the API Reference Docs. This tool aims to provide you a means of testing APIs with ease and convenience without having to switch between documentation and a testing tool to gain an understanding of API capabilities and responses.

## Test Request Widget
The widget is located within the “Try It Out” section in the documentation of each API endpoint.

![Test request](../assets/images/stoplight_try_it_out_request_001.png)


The Request URL and the HTTP Request Method are pre-filled based on the resource and the request type and cannot be edited. Most wflow APIs support OAuth tokens for authentication and will require you to generate an OAuth token before making API calls to the wflow service.

## Example Test Request
For instance, to make a [Users list]() API call, follow the following steps:

1. Before using OAuth with your application, you must register your application with the service. This is done through a technical support request. If you already have an existing unpublished OAuth app, skip to step 2.
2. In the following request widget [Get access token widget](#get-access-token-widget), provide your app’s Client ID and Secret. After providing this information, click the “Send” button. An OAuth access token will then be retrieved that can be used to make requests.
3. Since the [Users list]() does not require a request body, additional request information is not needed. Optionally, you can provide the value for the query parameters in the “Query” window of the widget to filter the response section. Click the “Send” button to make the request.

![Request sample](../assets/images/stoplight_try_it_out_request_001.png)

The response body will be displayed in the “Body” window as shown below:

![Response sample](../assets/images/stoplight_try_it_out_response_001.png)
### Test request widget
Fill in the following information in the widget and click “Send”:

**Headers section:**

Key | Value | Note
---------|----------|
 Authorization | Bearer *your access token* | [Try get access token here](#get-access-token-widget)

```json http
{
  "method": "get",
  "url": "https://api.wflow.com/api/user/myorganizations",
  "headers": {
    "Authorization": "Bearer "
  }
}
```

### Get access token widget

Fill in the following information in the widget:

**Headers section:**

Select **x-www-form-urlencoded** option in **Body section** and agree with Content-Type setting in header or set manually in Headers section:

Key | Value 
---------|----------
 Content-Type | **`application/x-www-form-urlencoded`** |

**Body section:**


Key | Value | Note
---------|----------|----------
 client_id | *your_wflow_non_interactive_client_name* | if you don't already have it, ask the customer administrator to create one or ask for technical support
 client_secret | *your_secret* | if you don't already have it, ask the customer administrator to create one or ask for technical support
 grant_type | **`client_credentials`** ||
 scope | **`uccl_common_api`** ||

```json http
{
  "method": "post",
  "url": "https://account.wflow.com/connect/token",
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "body": "client_id=your_wflow_non_interactive_client_name&client_secret=your_secret&grant_type=client_credentials&scope=uccl_common_api"
}
```
>Note
>
>Copy and paste the access token from the response to [jwt.io](https://jwt.io/) to inspect the raw token.

---
## Need support ?

Please email us at [developersupport@wflow.com](mailto:developersupport@wflow.com)



