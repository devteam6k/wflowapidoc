---
tags: [use cases, examples, how it works]
---
# How wflow.com works

## Basic flow
1. **Invoices or receipts are uploaded to wflow.com**</br>
The supplier can submit the primary documents (PDF, image,...) via a collection email. In addition, documents can be uploaded via the web interface or photographed by the wflow.com mobile application or via the API [(example)](#add-document--upload-files).
</br>
2. **Data is extracted**</br>
All data from electronic invoices and receipts is extracted in the Rossum application. If you want to use another alternative extraction method, you can do so using the API [(example)](#extraction-of-documents).
</br>
3. **Structured data is transferred to wflow.com**</br>
The structured data (i.e.extracted,...) is stored via the API [(example)](#update-document).
</br>
4. **Invoice approval process**</br>
The user(s) approve the invoice or receipt (may have multiple levels of approval).
</br>
5. **Export to ERP**</br>
After completing the approval workflow, the data is integrated into the ERP via an API [(example)](#export-to-erp).
</br>
## Import reference tables (registers)
Reference table import is used to maintain integrity between wflow.com and external sources (ERPs, ...). Import adds or updates the table entries. The existence of the entry is verified on the basis of the external Id. If it is not filled in, the code will be used in its place. The whole set is always updated. Entries that are not in the imported list are marked as invalid in the destination list. More in this [example](#import-cost-centers-reference-table).
## Examples
### Add document & upload file(s)
The following example shows you how to add a document (incoming invoice) to wflow.com and upload the associated file(s) to it.
Creating a document with attached file(s) must be done in 2 steps:
1. [Add document](#step-1-add-a-document)
2. [Upload file](#step-2-upload-file)
#### Step 1 Add a document
In this step, a new document is created, which may or may not contain structured data. If structured data will be obtained by extraction, then it is enough to create a document that contains only mandatory fields ("Document type Id" or "Document type Kind"). If the document is created successfully, it returns the `Id` of the created document. We will use this `Id` in [Step 2 Upload file](#step-2-upload-file).

```json
var data = JSON.stringify({
  "type": {
    "kind": "IncomingInvoice"
  }
});

var accessToken = your access token;
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // New document Id as Guid
  }
});

xhr.open("PUT", "https://api.wflow.com/api/demo/documents");
xhr.setRequestHeader("content-type", "application/json");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);
```
##### Try it
```json http
{
  "method": "put",
  "url": "https://api.wflow.com/api/{organization}/documents",
  "headers": {
    "Content-Type": "application/json",
    "Authorization": "Bearer "
  },
  "body": "{\n  \"type\": {\n    \"id\": \"give a real document type guid or omit\",\n    \"kind\": \"IncomingInvoice\"\n  }\n}"
}
```
#### Step 2 Upload file
In this step, we will upload the file to an existing (eg added above) document and mark it as **main** file.</br>
Suppose we received the following Document `Id` in Step 1: 3fa85f64-5717-4562-b3fc-2c963f66afa6. The data of the uploaded file must have the corresponding standard format.
```json
var fileContent = 'sample text'; // As a sample, upload a text file.
var file = new Blob([fileContent], {type: 'text/plain'});
var data = new FormData();
data.append("file.txt", file);

var accessToken = your access token;
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (xhr.readyState == 4 && xhr.status == 200)
  {
    console.log("File uploaded!");
    console.log(xhr.response); // Uploaded file Id (Guid)
  }
});

xhr.open("PUT", "https://api.wflow.com/api/demo/documents/3fa85f64-5717-4562-b3fc-2c963f66afa6/files/upload?main=true");
xhr.setRequestHeader("content-type", "multipart/form-data");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);
```
##### Try it
In the Body section, select Binary and select the uploaded file from your repository.
```json http
{
  "method": "put",
  "url": "https://api.wflow.com/api/{organization}/documents/{documentId}/files/upload",
  "headers": {
    "Content-Type": "multipart/form-data"
    "Authorization": "Bearer "
  },
  "query": {
    "main": "true"
  },
}
```
### Extraction of documents
This task is more complex and consists of the following logical subsequent steps:
1. [Get a list of documents ready for extraction](#step-1-get-a-list-of-documents-ready-for-extraction)
2. [Download the main files whose contents will be extracted](#step-2-download-the-main-files)
3. [Pass processing status information back to wflow.com](#step-3-pass-processing-status-information-back-to-wflowcom)
4. [Update extracted structured data into a document](#step-4-update-extracted-structured-data-into-a-document)
#### Step 1 Get a list of documents ready for extraction
In this step, we get a list of document Ids that are ready to be extracted. We will then use this list in the next [Step 2 Download the main files](#step-2-download-the-main-files) to gradually obtain the files whose contents will be extracted.

```json
var accessToken = your access token;
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // Array of document Ids
  }
});

xhr.open("GET", "https://api.wflow.com/api/organization/documents/toextract");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);

```
##### Try it
```json http
{
  "method": "get",
  "url": "https://api.wflow.com/api/{organization}/documents/toextract",
  "headers": {
    "Authorization": "Bearer "
  }
}
```
#### Step 2 Download the main files
In this step, we will download the main files of each document by iterating through the document Ids field, which we obtained in the previous [Step 1 Get a list of documents ready for extraction](#step-1-get-a-list-of-documents-ready-for-extraction). For simplicity, we will download the file for only one document. In the real case, this trigger code will cycle for all of the Ids array.
```json
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // Downloaded file content (blob)
  }
});

xhr.open("GET", "https://api.wflow.com/api/demo/documents/3fa85f64-5717-4562-b3fc-2c963f66afa6/files/main/download");
xhr.setRequestHeader("authorization", "Bearer ");

xhr.send(data);
```
##### Try it
```json http
{
  "method": "get",
  "url": "https://api.wflow.com/api/{organization}/documents/{documentId}/files/main/download",
  "headers": {
    "Authorization": "Bearer "
  }
}
```
#### Step 3 Pass processing status information back to wflow.com
In this step, we will inform wflow.com of the result of the file processing. The result can have two states: success or failure. In case of failure, you can optionally also send a text description of the reasons for the failure, which is highly recommended. This information is then available to end users. For simplicity, we will only inform about the result of processing one document. In fact, you do this for all documents you process.
```json
var accessToken = your access token;
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // No result
  }
});

xhr.open("PUT", "https://api.wflow.com/api/organization/documents/documentId/task/extract/processed?success=true");

/// xhr.open("PUT", "https://api.wflow.com/api/organization/documents/documentId/task/extract/processed?success=false&message=The%20message%20in%20case%20of%20failure");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);

```
##### Try it
```json http
{
  "method": "put",
  "url": "https://api.wflow.com/api/{organization}/documents/{documentId}/task/extract/processed",
  "headers": {
    "Authorization": "Bearer "
  },
  "query": {
    "success": "true",
    "message": "The message in case of failure"
  }
}
```
#### Step 4 Update extracted structured data into a document
This step is already described in this example [Update document](#update-document)
### Update document
The following example shows you how to update the document (incoming invoice) in wflow.com.
</br>
We will try to edit the document we added in the previous example. Specifically, we edit the `Invoice Number` field. In order to edit the document, we need to fill in the `Id` field. In our case, it will be 3fa85f64-5717-4562-b3fc-2c963f66afa6.
```json
var data = JSON.stringify({
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "number": "202000051"
});

var accessToken = your access token;
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // Updated document Id (Guid)
  }
});

xhr.open("PUT", "https://api.wflow.com/api/demo/documents");
xhr.setRequestHeader("content-type", "application/json");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);
```
##### Try it
```json http
{
  "method": "put",
  "url": "https://api.wflow.com/api/{organization}/documents",
  "headers": {
    "Content-Type": "application/json",
    "Authorization": "Bearer "
  },
  "body": "{\n  \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"\n  \"number\": \"202000051\"\n}"
}
```
### Export to ERP
This task is more complex and consists of the following logical subsequent steps:
1. [Get a list of documents ready for export to ERP](#step-1-get-a-list-of-documents-ready-for-export-to-erp)
2. [Get document data for using in ERP](#step-2-get-document-data)
3. [Optional - download the main file](#step-3-download-the-main-file---optional)
4. [Pass processing status information back to wflow.com](#step-4-pass-processing-status-information-back-to-wflowcom)
5. [Optional - update document data that is somehow related to ERP](#step-5-update-document-data-that-is-somehow-related-to-erp)
#### Step 1 Get a list of documents ready for export to ERP
In this step, we get a list of document Ids that are ready to be exported to ERP. We will then use this list in the next [Step 2 Get document data for using in ERP](#) or [Step 3 Download the main file](#) to gradually obtain the data or files whose contents will be exported to ERP.

```json
var accessToken = your access token;
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // Array of document Ids
  }
});

xhr.open("GET", "https://api.wflow.com/api/organization/documents/toexport");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);

```
##### Try it
```json http
{
  "method": "get",
  "url": "https://api.wflow.com/api/{organization}/documents/toexport",
  "headers": {
    "Authorization": "Bearer "
  }
}
```
#### Step 2 Get document data
In this step, we obtain structured document data of each document by iterating through the document Ids field, which we obtained in the previous [Step 1 Get a list of documents ready for export to ERP](#step-1-get-a-list-of-documents-ready-for-export-to-erp). For simplicity, we will get data for only one document. In the real case, this trigger code will cycle for all of the Ids array. This data can be used for any purpose - in our case for writing to ERP.
```json
var accessToken = your access token;
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // Document object
  }
});

xhr.open("GET", "https://api.wflow.com/api/organization/documents/3fa85f64-5717-4562-b3fc-2c963f66afa6");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);
```
##### Try it
```json http
{
  "method": "get",
  "url": "https://api.wflow.com/api/{organization}/documents/{documentId}",
  "headers": {
    "Authorization": "Bearer "
  }
}
```
#### Step 3 Download the main file - optional
In this step, we will download the main files of each document by iterating through the document Ids field, which we obtained in the previous [Step 1 Get a list of documents ready for export to ERP](#step-1-get-a-list-of-documents-ready-for-export-to-erp). For simplicity, we will download the file for only one document. In the real case, this trigger code will cycle for all of the Ids array.
```json
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // Downladed file content (blob)
  }
});

xhr.open("GET", "https://api.wflow.com/api/demo/documents/3fa85f64-5717-4562-b3fc-2c963f66afa6/files/main/download");
xhr.setRequestHeader("authorization", "Bearer ");

xhr.send(data);
```
##### Try it
```json http
{
  "method": "get",
  "url": "https://api.wflow.com/api/{organization}/documents/{documentId}/files/main/download",
  "headers": {
    "Authorization": "Bearer "
  }
}
```
#### Step 4 Pass processing status information back to wflow.com
In this step, we will inform wflow.com of the result of the file processing. The result can have two states: success or failure. In case of failure, you can optionally also send a text description of the reasons for the failure, which is highly recommended. This information is then available to end users. For simplicity, we will only inform about the result of processing one document. In fact, you do this for all documents you process.
```json
var accessToken = your access token;
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && xhr.status == 200) {
    console.log(xhr.response); // No result
  }
});

xhr.open("PUT", "https://api.wflow.com/api/organization/documents/documentId/task/export/processed?success=true");

/// xhr.open("PUT", "https://api.wflow.com/api/organization/documents/documentId/task/export/processed?success=false&message=The%20message%20in%20case%20of%20failure");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);

```
##### Try it
```json http
{
  "method": "put",
  "url": "https://api.wflow.com/api/{organization}/documents/{documentId}/task/export/processed",
  "headers": {
    "Authorization": "Bearer "
  },
  "query": {
    "success": "true",
    "message": "The message in case of failure"
  }
}
```
#### Step 5 Update document data that is somehow related to ERP
This step is already described in this example [Update document](#update-document)
### Import Cost centers reference table
In this example, we will import the Cost center reference table into wflow.com. The first two table entries will be imported as valid, the last as invalid.
```json
var data = JSON.stringify([
  {
    "externalId": "cs00001",
    "code": "cs00001",
    "description": "Cost center 00001",
    "isValid": true
  },
  {
    "externalId": "cs00002",
    "code": "cs00002",
    "description": "Cost center 00002",
    "isValid": true
  },
  {
    "externalId": "xx415444",
    "code": "cs00002",
    "description": "Invalid Cost center 00002",
    "isValid": false
  }
]);

var accessToken = your access token;
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("PUT", "https://api.wflow.com/api/demo/registers/costcenters");
xhr.setRequestHeader("content-type", "application/json");
xhr.setRequestHeader("authorization", "Bearer " + accessToken);

xhr.send(data);
```
##### Try it
```json http
{
  "method": "put",
  "url": "https://api.wflow.com/api/{organization}/registers/costcenters",
  "headers": {
    "Content-Type": "application/json",
    "Authorization": "Bearer "
  },
  "body": [
    {
      "externalId": "cs00001",
      "code": "cs00001",
      "description": "Cost center 00001",
      "isValid": true
    },
    {
      "externalId": "cs00002",
      "code": "cs00002",
      "description": "Cost center 00002",
      "isValid": true
    },
    {
      "externalId": "xx415444",
      "code": "cs00002",
      "description": "Invalid Cost center 00002",
      "isValid": false
    }
  ]
}
```
---
## Need support ?

Please email us at [developersupport@wflow.com](mailto:developersupport@wflow.com)

