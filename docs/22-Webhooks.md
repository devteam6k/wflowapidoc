---
stoplight-id: bmfos7pbfi2vz```json

```
---

# Webhooks

wflow.com service can be configured to send a web request to an external website whenever an event is triggered. The external website can be programmed to do whatever is needed when it receives the request, such as log the event, send an email, or perform a data-related activity. This ability to receive a request saves external sites from having to repeatedly poll and inspect the feature service for changes.

To receive webhook requests, it is necessary to set up via API endpoints, the description of which can be found in the [Webhooks Registration](https://developers.wflow.com/docs/wflowapidoc/fa1d8905377b1-add-or-update-webhook-registration) section. Each webhook identifies the external URL the request gets sent to, as well as change event that trigger it.

From the moment you have a registered webhook subscription, corresponding requests are sent to the URL that was specified in the registration. The [payload](#payloads) of these requests contains all the necessary data for subsequent processing. The payload model varies depending on the type of event that triggered the webhook to be sent.

Webhook events:

Action | Description
-------|----------
`DocumentReadyToExtract`|Document is ready to send for extraction
`DocumentReadyToExport`|Document is ready to export (to ERP)
`DocumentUpdated`|Document has been updated
`DocumentApprovalProcessFinished`|Document approval process has been fully completed
`DocumentDeleted`|Document has been deleted
`DocumentReadyToReview`|Document is ready to review
`DocumentCreated`|Document has been created
`DocumentReviewed`|Document has been reviewed
`RegisterUpdated`|Register collection has been updated


Use `All` when you want to subscribe all events.


## Payloads

Each event type has a specific payload format with the relevant event information. In addition to the information that is relevant to each event, all event webhook payloads include information about the organization that the event occurred on and webhook registration data.

### Document events payload

action: `DocumentReadyToExtract`
`DocumentReadyToExport`
`DocumentUpdated`
`DocumentDeleted`
`DocumentReadyToReview`
`DocumentCreated`
`DocumentReviewed`

```json
{
  "notification": {
    "organization": "org1-customer",
    "action": "DocumentReadyToExport",
    "documentId": "463b5265-2672-4191-9e3b-9d6fcfe6a452"
  },
  "registrationId": "175b0efe-fb2c-4d03-8f9e-532776195eae",
  "description": "My webhook name",
  "id": "5f2ff4ca-0c22-42b0-9705-12bc2f8b4f94"
}
```

### Document approval process events payload

action: `DocumentApprovalProcessFinished`
<br>
status: `Approved, Rejected, Returned`

```json
{
  "notification": {
    "organization": "org1-customer",
    "action": "DocumentApprovalProcessFinished",
    "documentId": "463b5265-2672-4191-9e3b-9d6fcfe6a452",
    "status": "Approved"
  },
  "registrationId": "175b0efe-fb2c-4d03-8f9e-532776195eae",
  "description": "My webhook name",
  "id": "5f2ff4ca-0c22-42b0-9705-12bc2f8b4f94"
}
```

### Registers events payload

action: `RegisterUpdated`
<br>
registerType: `DocumentTypes`
`Series`
`CostCenters`
`Contracts`
`Activities`
`AccountingRules`
`ChartOfAccounts`
`VATControlStatementLines`
`VATReturnLines`
`VATReverseChargeCodes`
`PaymentMethods`
`CardDocumentTypes`
`CashDocumentTypes`
`CashRegisters`
`BusinessItems`
`Partners`
`MeasureUnits`
`Employees`
`Vehicles`
`OrganizationPersons`
`PartnerPersons`

```json
{
  "notification": {
    "organization": "org1-customer",
    "action": "RegisterUpdated",
    "registerType": "CostCenters"
  },
  "registrationId": "175b0efe-fb2c-4d03-8f9e-532776195eae",
  "description": "My webhook name",
  "id": "5f2ff4ca-0c22-42b0-9705-12bc2f8b4f94"
}
```
