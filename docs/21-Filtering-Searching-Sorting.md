---
tags: [filtering, searching]
---

# Filtering & Searching, Sorting

## Filtering & Searching

The query parameter ***"query"*** is used for filtering/searching in all API endpoints that return a collection. The ***"query"*** can contain an expression or a set of expressions that is used to define a condition for filtering/searching required items. The expression format looks like this:

```json
query_term operator value
```

Where:

***query_term*** is the query term or field to search upon. Any field that is contained in the corresponding response model can be used as query terms.

***operator*** specifies the condition for the query term. Query operators can be "=", "<", "<=", ">", "> =", "! =".

**value** is the specific value you want to use to filter your search results. Value for Text and Date / DateTime. The Value can also be null. If you use null in the value, then it is possible to use only "=" and "!=" in the operator

Basic rules:
- ***query_term*** must be enclosed in quotation marks.
- there must be 1 space between **query_term**, **operator** and **value**.<p>
*Examples of invalid inputs:* `?query=code="A100"` or `?query=code ="A100"` or `?query=code= "A100"`<p>
*Valid input:* `?query=code = "A100"` or `?query=code = null`


### Examples

1.	The following query string filters the search to just return only item (i.e. Cost center) with code equals to A100:

`?query=code = "A100"`


2.	The following query string filters the search to just return only items (i.e. Documents) with a total amount of more than 1000 and less than 5000:

`?query=totalAmount > 1000 and totalAmount < 5000`

3.  The following query string filters the search to return only items (that is, Documents) with a creation date later than January 1st 2021:

`?query=created > "2021-01-01"`

4.  The following query string filters the search to return only items (that is, Documents) with a empty number:

`?query=number = null`
## Sorting

The query parameter ***"sort"*** is used for a comma-separated list of sort keys. Valid keys are all fields that are contained in the corresponding response model. Each key sorts ascending by default, but may be reversed with the 'desc' modifier.

### Examples

`?sort=created,updated desc,partnerName`
