# Integration

## Integromat sample ("Authorization code" grant type)

As an example of setting up integration via Authorization code, we present an example from creating a scenario in the Integromat service. This simple example shows how to set up OAuth 2 for an HTTP request. For simplicity, we do not describe the possibilities of the Integromat service here, because this is not the subject of this article.

Here are the steps:

### Create new scenario
In the Scenarios section, press the Create a new scenario button. A page appears where you can select the service you want to include in the scenario. Enter "HTTP" in the Search field. Click on the displayed icon with the caption HTTP. Click on the displayed icon with the caption "HTTP" and press the Continue button.
<br>
![Create scenario](../assets/images/integromat_select_service.png)
<br>
The workspace of the new scenario appears:
</br>
![New scenario](../assets/images/integromat_new_scenario.png)
<br>
Click on the displayed icon with the caption "HTTP" and then select "Make an OAuth 2.0 request":
</br>
![Select OAuth 2.0 request](../assets/images/integromat_select_oauth2_request.png)
### Add connection
In the HTTP form that appears then press "Add" next to the "Connection" field:
</br>
![Add connection](../assets/images/integromat_add_connection.png)
</br>
The "Create connection" form is displayed. Fill in or set the following fields as follows:
* **Connection name**: Enter the connection name you want
* **Flow type**: select "Authorization code"
* **Authorize URI**: https://account.wflow.com/connect/authorize
* **Token URI**: https://account.wflow.com/connect/token
* **Scope**: add "uccl_common_api" (to get API access) and "offline_access" (to get refresh_token)
* **Scope separator**: select "SPACE". To make this setting, you must check the "Show advanced settings" checkbox on the form (at the bottom).
* **Client ID**: enter the client ID you created for Integromat integration needs here. The client must be created with the "Authorization code" grant type and its "Redirect URI whitelist" must contain: https://www.integromat.com/oauth/cb/oauth2 (more info here [How to connect Integromat to any web service that uses OAuth2 authorization](https://support.integromat.com/hc/en-us/articles/115003864294)). Instructions about wflow.com client registration can be found here [Client registration](01-Using-API.md#client-registration)
* **Client secret**: enter the client secret you created for Integromat integration needs here.

![Setup connection](../assets/images/integromat_setup_connection.png)
</br>
Then press "Continue" button. Depending on whether or not the corresponding user is logged in, the login page is displayed.
### Login
![Login user](../assets/images/wflow_login_page.png)
</br>
After a successful login or if the user is already logged in, the Conset page is displayed. Here you can see the scopes required by Integromat. If you agree, press the "Yes, Allow" button.<br>
![Consent screen](../assets/images/integromat_consent_screen.png)
</br>
This completes the connection setup and saves it. You can manage the connection in the "Connections" section.
### Setup request
All that remains is to "just" set the required request press OK button and it's done. Documentation of available API endpoints can be found here [Wflow API Documentation](../reference/WflowOpenAPI.json).
</br>
![Setup request](../assets/images/integromat_setup_request.png)
</br>
## Need support ?

Please email us at [developersupport@wflow.com](mailto:developersupport@wflow.com)